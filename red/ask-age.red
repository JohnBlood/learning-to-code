Red [needs: view]
view [
  f1: field "First name"
  f2: field "Last name"
  button "Greet me!" [
    t1/text: rejoin ["Have a nice day " f1/text " " f2/text "!"]
  ]
  return
  t1: text "" 200

]