REM Cascading numbers
REM by John Paul Wohlscheid

REM ask for numbers
PRINT "Please enter four numbers and watch the values cacade"
INPUT "Enter your first number: "; num1%
INPUT "Enter your second number: "; num2%
INPUT "Enter your third number: "; num3%
INPUT "Enter your fourth number: "; num4%

ab% = num1% - num2%
cd% = num3% - num4%
finalnum% = ab% - cd%

REM displaying numbers while using str$ to convert numbers to strings
CLS
PRINT STR$(num1%) + "           " + STR$(num2%) + "           " + STR$(num3%) + "           " + STR$(num4%)
PRINT "   \         /              \         /"
PRINT "    \       /                \       /"
PRINT "     \     /                  \     /"
PRINT "      \   /                    \   /"
PRINT "       \ /                      \ /"
PRINT
print "       " + STR$(ab%) + "                       " + STR$(cd%)
PRINT
print "         \                      /"
PRINT "          \                    /"
print "           \                  /"
print "            \                /"
PRINT "             \              /"
PRINT "              \            /"
PRINT "               \          /"
print "                \        /"
PRINT "                 \      /"
PRINT "                  \    /"
PRINT "                   \  /"
print "                    \/"
PRINT
print "                    " + STR$(finalnum%)
PRINT "=========================================================="