start:
CLS
PRINT "This program will help you determine"
PRINT "if you are old enough to apply for"
PRINT "Social Security."
PRINT
INPUT "Please enter your current age."; age
IF age < 65 THEN GOSUB young:
IF age > 65 THEN GOSUB old:
IF age = 65 THEN GOSUB correct:
GOTO start

young:
CLS
PRINT "You are too young. Please try again"
PRINT "in a couple of years."
PRINT
INPUT "Press 'Enter' to continue."; dummyVariable
PRINT
GOTO start

old:
CLS
PRINT "You are should have applied for"
PRINT "Social Security by now."
PRINT
INPUT "Press 'Enter' to continue."; dummyVariable
PRINT
GOTO start

correct:
CLS
PRINT "You are now eligiable to apply for "
PRINT "Social Security."
PRINT
INPUT "Press 'Enter' to continue."; dummyVariable
PRINT
GOTO start

