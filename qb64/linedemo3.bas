'-------------------------
'-LINE demonstration #3 -
'-------------------------

CONST YELLOW = _RGB32(255, 255, 0)
dim Style&
dim Bit%
dim Dir%

Dir% = -1

SCREEN _NEWIMAGE(640, 480, 32)
do
	cls
	_LIMIT 30
	LOCATE 2, 33
	PRINT "A Stylized Line"
	LOCATE 4, 21
	PRINT "Press the Space Bar to change directions"
	LOCATE 6, 23
	PRINT "Press the Escape key to exit the program."
	if _KEYHIT = 32 THEN Dir% = -Dir%
	Bit% = Bit% + Dir%
	if Bit% = 16 THEN
		Bit% = 0
	ELSEIF Bit% = -1 then
		Bit% = 15
	End if
	Style& = 2 ^ Bit%
	LINE (99, 129)-(539, 409), YELLOW, B , Style&
	_DISPLAY
LOOP UNTIL _KEYDOWN(27)