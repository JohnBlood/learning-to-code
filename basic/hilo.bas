' Here is an interactive HI-LO
' Program
[start]
    guessMe = int(rnd(1)*100) + 1
    'Clear the screen and print the title and instructions
    cls
    print
    print HI-LO
    print
    print "I have decided on a number between"
    print "a hundred, and I want you to guess"
    print "what it is. I will tell you to guess"
    print "higher or lower, and we'll count up"
    print "the number of guesses you use."
    print

[ask]
    ' Ask the user to guess the number and tally the guess
    input "OK. What is your guess"; guess
    ' Now add one to the count variable to count the guesses
    let count = count + 1
    ' Check to see if the guess is right
    if guess = guessMe then goto [win]
    ' Check to see if the guess is too low
    if guess < guessMe then print "Guess higher."
    ' Check to see if the guess is too high
    if guess > guessMe then print "Guess lower"
    ' Go back and ask again
    goto [ask]
[win]
    ' Tell how many guess it took to win
    print "You win! It took "; count; " guesses."
    ' Reset the count variable to zero for the next game
    let ocunt = 0
    ' Ask to play again
    input "Play again (Y/N)"; play$
    if instr("YESyes", play$) > 0 then goto [start]
    print "Press ALT-F4 to close this window."
    end
