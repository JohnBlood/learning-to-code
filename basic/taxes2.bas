[start]
    print "Type a dollar and cent amount ?"
    input "(Press 'Enter' alone for help) "; valueA
    let tax = amount * 0.05
    
    
    
    
    
    let valueB = int(valueA)
    valueC = valueA - valueB
    
    goto [start]

[help]
    cls
    print "SALESTAX.BAS Help"
    print
    print "This tax program determines how much tax is"
    print "due on an amount entered and also computes"
    print "the total amount. The tax rate is 5%."
    print
    input "Press [Enter] to continue."; dummyVariable
    cls
    goto [start]
