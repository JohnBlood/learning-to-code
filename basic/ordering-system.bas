REM Order System 0.5 - FreeBasic Edition
REM by John Paul Wohlscheid

REM Variables

DIM items as integer 'item total
DIM fname as string 'customer first name
DIM lname as string 'customer last name
DIM total as integer 'order total

REM Main Program

PRINT TAB(15); "Welcome to the first version of this Order System."
PRINT TAB(15); "To place your order, enter the amount you of each"
PRINT TAB(15); "item you want and the amount will be totalled"
PRINT TAB(15); "at the end."
PRINT
PRINT TAB(15); "First of all, please enter your name."
INPUT "Enter your first name: ", fname
INPUT "Enter your last name: ", lname
PRINT "Thank you, " + fname + "!"
PRINT

