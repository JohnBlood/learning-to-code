'This program will ask for your age an triple it

[start]

    print "Please enter your age"
    input "(Press 'h' for help) ?" ; age
    'The above will capture the user's age and make it a variable
    if age = h then goto [help]
    let triple = age * 3
    print "Triple your age is: "; triple ; "."
    goto [start]

[help]

    cls
    print "In order for this highly complicated program"
    print "to determine triple your age, you need to enter"
    print "your current age."
    print
    input "Press [Enter] to continue."; dummyVariable
    print
    goto [start]
