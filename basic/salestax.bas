[start]
    print "Type a dollar and cent ammount."
    input "(Press 'Enter' alone for help) ?" ; amount
    if amount = 0 then goto [help]
    let tax = amount * 0.05
    print "Tax is: "; tax; ". Total is: "; tax + amount
    goto [start]
    
[help]
    cls
    print "SALETAX.BAS Help"
    print
    print "This tax program determines how much tax is"
    print "due on an amount entered and also computes"
    print "the total amount. The tax rate is 5%"
    print
    input "Press [Enter] to continue."; dummyVariable
    print
    goto [start]
