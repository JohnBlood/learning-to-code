[start]
    cls
    print "This program will help you determine"
    print "if you are old enough to apply for"
    print "Social Security."
    print
    input "Please enter your current age."; age
    if age < 65 then [young]
    if age > 65 then [old]
    if age = 65 then [correct]
    goto [start]

[young]
    cls
    print "You are too young. Please try again"
    print "in a couple of years."
    print
    input "Press 'Enter' to continue."; dummyVariable
    print
    goto [start]

[old]
    cls
    print "You are should have applied for"
    print "Social Security by now."
    print
    input "Press 'Enter' to continue."; dummyVariable
    print
    goto [start]

[correct]
    cls
    print "You are now eligiable to apply for "
    print "Social Security."
    print
    input "Press 'Enter' to continue."; dummyVariable
    print
    goto [start]
